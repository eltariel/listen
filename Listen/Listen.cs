﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Listen
{
    /// <summary>
    /// Listen to changes in the filesystem.
    /// </summary>
    public class Listen
    {
        private readonly IEnumerable<string> empty = Enumerable.Empty<string>();
        private readonly Dictionary<string, FileSystemWatcher> watchers;
        private readonly Action<IEnumerable<string>, IEnumerable<string>, IEnumerable<string>> changeAction;

        private Listen(IEnumerable<string> paths, Action<IEnumerable<string>, IEnumerable<string>, IEnumerable<string>> changeAction)
        {
            watchers = paths.ToDictionary(p => p, p => MakeWatcher(p));
            this.changeAction = changeAction;
        }

        /// <summary>
        /// Factory method.
        /// </summary>
        /// <param name="paths">Collection of paths to watch.</param>
        /// <param name="changeAction">Action to call on filesystem event..</param>
        /// <returns>A listener.</returns>
        public static Listen To(IEnumerable<string> paths, Action<IEnumerable<string>, IEnumerable<string>, IEnumerable<string>> changeAction)
        {
            return new Listen(paths, changeAction);
        }

        /// <summary>
        /// Start listening.
        /// </summary>
        public void Start()
        {
            foreach (var watcher in watchers.Values)
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// Stop listening.
        /// </summary>
        public void Stop()
        {
            foreach (var watcher in watchers.Values)
            {
                watcher.EnableRaisingEvents = false;
            }
        }

        private FileSystemWatcher MakeWatcher(string path)
        {
            var watcher = new FileSystemWatcher(path);
            watcher.Created += (_, e) => DoStuff(e);
            watcher.Changed += (_, e) => DoStuff(e);
            watcher.Deleted += (_, e) => DoStuff(e);
            watcher.Renamed += (_, e) => DoStuff(e);

            return watcher;
        }

        private void DoStuff(FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    changeAction(new[] { e.FullPath }, empty.ToArray(), empty.ToArray());
                    break;
                case WatcherChangeTypes.Changed:
                    changeAction(empty.ToArray(), new[] { e.FullPath }, empty.ToArray());
                    break;
                case WatcherChangeTypes.Renamed:
                    var re = e as RenamedEventArgs;
                    changeAction(new[] { re.FullPath }, empty.ToArray(), new[] { re.OldFullPath });
                    break;
                case WatcherChangeTypes.Deleted:
                    changeAction(empty.ToArray(), empty.ToArray(), new[] { e.FullPath });
                    break;
            }
        }
    }
}