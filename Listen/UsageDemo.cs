﻿using System;
using System.Threading;

namespace Listen
{
    public class UsageDemo
    {
        public static void Main()
        {
            var listener = Listen.To(new[] {@"C:\Work"},
                (created, changed, deleted) =>
                {
                    //created.Dump();
                    //changed.Dump();
                    //deleted.Dump();
                });

            listener.Start();

            Thread.Sleep(TimeSpan.FromMinutes(1));
        }
    }
}
